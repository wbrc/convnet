all: mnist cifar

mnist:
	go build -v -mod vendor -o $@ ./example/mnist

cifar:
	go build -v -mod vendor -o $@ ./example/cifar

