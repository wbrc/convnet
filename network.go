package convnet

import (
	"fmt"
	"log"
)

type Network struct {
	GlobalInitializer          Initializer
	GlobalOptimizerInitializer OptimizerInitializer
	layers                     []Layer
}

func (n *Network) Add(layer Layer) {
	n.layers = append(n.layers, layer)
}

func (n *Network) Initialize() {

	for i := range n.layers {
		n.layers[i].SetOptimizerInitializer(n.GlobalOptimizerInitializer)
	}

	for i := 1; i < len(n.layers); i++ {
		n.layers[i-1].SetDelta(n.layers[i].PreDelta())
		n.layers[i].SetInput(n.layers[i-1].Activation())
	}
	for i := 0; i < len(n.layers); i++ {
		n.layers[i].Initialize(n.GlobalInitializer)
	}
}

func (n *Network) Input() []float64 {
	return n.layers[0].Input()
}

func (n *Network) Output() []float64 {
	return n.layers[len(n.layers)-1].Activation()
}

func (n *Network) Delta() []float64 {
	return n.layers[len(n.layers)-1].Delta()
}

func (n *Network) Error() float64 {
	l, ok := n.layers[len(n.layers)-1].(ErrorLayer)
	if !ok {
		panic("last layer is not an error layer")
	}
	return l.Error()
}

func (n *Network) Forward() {
	for i := range n.layers {
		n.layers[i].Forward()
	}
}

func (n *Network) Backpropagate() {
	for i := len(n.layers) - 1; i >= 0; i-- {
		n.layers[i].Backpropagate()
	}
}

func (n *Network) CalculateGradients() {
	for i := len(n.layers) - 1; i >= 0; i-- {
		n.layers[i].CalculateGradients()
	}
}

func (n *Network) Update(learningRate float64) {
	for i := range n.layers {
		n.layers[i].Update(learningRate)
	}
}

func (n *Network) EnableTrainMode() {
	for _, layer := range n.layers {
		if l, ok := layer.(ModeToggler); ok {
			l.EnableTrainMode()
		}
	}
}
func (n *Network) EnablePredictMode() {
	for _, layer := range n.layers {
		if l, ok := layer.(ModeToggler); ok {
			l.EnablePredictMode()
		}
	}
}

func (n *Network) Draw() {
	for i, layer := range n.layers {
		if l, ok := layer.(Drawer); ok {
			l.Draw(fmt.Sprintf("%d", i))
		}
	}
}

func (n *Network) Load() {
	for i, layer := range n.layers {
		if l, ok := layer.(PersistableLayer); ok {
			err := l.Load()
			if err != nil {
				log.Printf("Unable to load layer %d: %s", i, err)
			} else {
				log.Printf("loaded layer %d: %s", i, l.Name())
			}
		}
	}
}

func (n *Network) Persist() {
	for i, layer := range n.layers {
		if l, ok := layer.(PersistableLayer); ok {
			err := l.Persist()
			if err != nil {
				log.Printf("Unable to persist layer %d: %s", i, err)
			} else {
				log.Printf("persisted layer %d: %s", i, l.Name())
			}
		}
	}
}
