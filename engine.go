package convnet

import (
	"fmt"
	"math/rand"

	"gitlab.com/wbrc/progress"
)

type Engine struct {
	Epoch         int
	LrSchedule    func(epoch int) float64
	Model         *Network
	TrainData     *Dataset
	TestData      *Dataset
	BatchCallback func(batchId int, trainErr, testErr float64)
	EpochCallback func(epoch int, epochErr, accuracy float64)
}

func (engine *Engine) Start() {
	rawInput := engine.Model.Input()
	rawOutput := engine.Model.Output()
	rawDelta := engine.Model.Delta()

	epochSeries := progress.Series(engine.Epoch)

	for epoch := 0; epoch < engine.Epoch; epoch++ {
		fmt.Printf("Epoch %s ", epochSeries())
		bar := progress.Bar(engine.TrainData.Count())
		engine.TrainData.Shuffle()
		engine.TestData.Shuffle()
		for batch := 0; batch < engine.TrainData.Count(); batch++ {
			batchIn, batchOut := engine.TrainData.Get(batch)

			offsetInput := 0
			offsetOutput := 0
			for i := range batchIn {
				offsetInput += copy(rawInput[offsetInput:], batchIn[i])
				offsetOutput += copy(rawDelta[offsetOutput:], batchOut[i])
			}

			engine.Model.Forward()
			engine.Model.Backpropagate()
			engine.Model.CalculateGradients()
			engine.Model.Update(engine.LrSchedule(epoch))
			trainErr := engine.Model.Error()

			engine.Model.EnablePredictMode()
			testBatchIn, testBatchOut := engine.TestData.Get(rand.Intn(engine.TestData.Count()))
			offsetInput = 0
			offsetOutput = 0
			for i := range testBatchIn {
				offsetInput += copy(rawInput[offsetInput:], testBatchIn[i])
				offsetOutput += copy(rawDelta[offsetOutput:], testBatchOut[i])
			}
			engine.Model.Forward()
			testErr := engine.Model.Error()
			engine.Model.EnableTrainMode()

			engine.BatchCallback(epoch*engine.TrainData.Count()+batch, trainErr, testErr)
			bar(1)
		}

		engine.Model.EnablePredictMode()
		epochError := 0.0
		correct, all := 0, 0
		for batch := 0; batch < engine.TestData.Count(); batch++ {
			batchIn, batchOut := engine.TestData.Get(batch)

			offsetInput := 0
			offsetOutput := 0
			for i := range batchIn {
				offsetInput += copy(rawInput[offsetInput:], batchIn[i])
				offsetOutput += copy(rawDelta[offsetOutput:], batchOut[i])
			}

			engine.Model.Forward()
			epochError += engine.Model.Error()

			for i := range batchOut {
				if getMax(batchOut[i]) == getMax(rawOutput[i*len(batchOut[i]):i*len(batchOut[i])+len(batchOut[i])]) {
					correct++
				}
				all++
			}
		}
		engine.Model.EnableTrainMode()
		epochError /= float64(engine.TestData.Count())
		engine.EpochCallback(epoch, epochError, float64(correct)/float64(all))
		fmt.Println()

		engine.Model.Draw()
	}
}

func getMax(data []float64) int {
	max := data[0]
	maxIndex := 0
	for i, e := range data[1:] {
		if e > max {
			max = e
			maxIndex = i + 1
		}
	}
	return maxIndex
}
