package convnet

import (
	"math/rand"
)

type Dataset struct {
	BatchSize int
	images    [][]float64
	outputs   [][]float64
}

func (ds *Dataset) Add(img []float64, out []float64) {
	ds.images = append(ds.images, img)
	ds.outputs = append(ds.outputs, out)
}

func (ds *Dataset) Shuffle() {
	hi := len(ds.images)
	for hi > 0 {
		index := rand.Intn(hi)
		ds.images[index], ds.images[hi-1] = ds.images[hi-1], ds.images[index]
		ds.outputs[index], ds.outputs[hi-1] = ds.outputs[hi-1], ds.outputs[index]
		hi--
	}
}

func (ds *Dataset) Count() int {
	return len(ds.images) / ds.BatchSize
}

func (ds *Dataset) Get(index int) ([][]float64, [][]float64) {
	lo, hi := index*ds.BatchSize, index*ds.BatchSize+ds.BatchSize
	return ds.images[lo:hi], ds.outputs[lo:hi]
}
