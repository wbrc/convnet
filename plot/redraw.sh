#!/bin/sh
plot/prepare_epoch.awk < train.log > progress.log
plot/prepare_error.awk < train.log > stat.dat

if [ $(wc -l progress.log | cut -d' ' -f1) -gt 1 ]; then
    plot/plot_epoch.gp
fi

plot/plot_error.R
