#!/usr/bin/gnuplot

set terminal svg enhanced background rgb "white"
set output "epoch.svg"
set ytics nomirror
set y2tics
plot "progress.log" u 1:2 w lines t "Error", "progress.log" u 1:3 w lines \
    t "Accuracy" axis x1y2