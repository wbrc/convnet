#!/usr/bin/awk -f

/^[0-9]{4}\/[0-9]{2}.*Epoch/ {
    gsub(/:/, "");
    print $4, $6, $8
}
