package convnet

type ActivationFunction interface {
	F(float64) float64
	D(float64) float64
}
