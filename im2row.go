package convnet

func TransformColview(input, output []float64, batchSize, featureMaps,
	featureMapSize int) {

	if len(input) != len(output) {
		panic("slice length mismatch")
	}

	bMul := featureMapSize * featureMapSize * featureMaps
	inputIndex := 0

	for b := 0; b < batchSize; b++ {
		for fm := 0; fm < featureMaps; fm++ {
			for row := 0; row < featureMapSize; row++ {
				for col := 0; col < featureMapSize; col++ {
					outputIndex := b*bMul + (row*featureMapSize+col)*featureMaps + fm
					output[outputIndex] = input[inputIndex]
					inputIndex++
				}
			}
		}
	}
}

func InputGetPixel(input []float64, featureMaps, featureMapSize, pad, b, fm, row, col int) float64 {
	row -= pad
	col -= pad
	if row < 0 || col < 0 || row >= featureMapSize || col >= featureMapSize {
		return 0
	}
	return input[b*featureMapSize*featureMapSize*featureMaps+(row*featureMapSize+col)*featureMaps+fm]
}

func Im2Row(input, output []float64, batchSize, featureMaps, featureMapSize, kernelSize, stride, pad int) {

	outputIndex := 0
	for b := 0; b < batchSize; b++ {
		for row := 0; row < featureMapSize-kernelSize+2*pad+1; row += stride {
			for col := 0; col < featureMapSize-kernelSize+2*pad+1; col += stride {
				for fm := 0; fm < featureMaps; fm++ {
					for kRow := 0; kRow < kernelSize; kRow++ {
						for kCol := 0; kCol < kernelSize; kCol++ {
							output[outputIndex] = InputGetPixel(input, featureMaps, featureMapSize, pad, b, fm, row+kRow, col+kCol)
							outputIndex++
						}
					}
				}
			}
		}
	}
}

func InputAddPixel(input []float64, val float64, featureMaps, featureMapSize, pad, b, fm, row, col int) {
	row -= pad
	col -= pad
	if row < 0 || col < 0 || row >= featureMapSize || col >= featureMapSize {
		return
	}
	input[b*featureMapSize*featureMapSize*featureMaps+(row*featureMapSize+col)*featureMaps+fm] += val
}

func Row2Im(input, output []float64, batchSize, featureMaps, featureMapSize, kernelSize, stride, pad int) {
	outputIndex := 0
	for b := 0; b < batchSize; b++ {
		for row := 0; row < featureMapSize-kernelSize+2*pad+1; row += stride {
			for col := 0; col < featureMapSize-kernelSize+2*pad+1; col += stride {
				for fm := 0; fm < featureMaps; fm++ {
					for kRow := 0; kRow < kernelSize; kRow++ {
						for kCol := 0; kCol < kernelSize; kCol++ {
							InputAddPixel(input, output[outputIndex], featureMaps, featureMapSize, pad, b, fm, row+kRow, col+kCol)
							outputIndex++
						}
					}
				}
			}
		}
	}
}

