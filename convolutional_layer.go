package convnet

import (
	"bufio"
	"compress/gzip"
	"encoding/binary"
	"fmt"
	"os"

	"gonum.org/v1/gonum/mat"
)

type ConvolutionalLayer struct {
	batchSize, inputDepth    int
	inputWidth, kernelCount  int
	kernelWidth, stride, pad int

	rawInput               []float64 // bs*indep*inwid*inwid (colview)
	rawPreDelta            []float64 // bs*indep*inwid*inwid (colview)
	rawInputTransformed    []float64 // bs*((iw-kw+2*p)/s + 1)^2 * kw*kw*id (rowview)
	rawPreDeltaTransformed []float64 // bs*((iw-kw+2*p)/s + 1)^2 * kw*kw*id (rowview)
	rawOutput              []float64 // bs*((iw-kw+2*p)/s + 1)^2 * kcount
	rawActivation          []float64 // bs*((iw-kw+2*p)/s + 1)^2 * kcount
	rawDelta               []float64 // bs*((iw-kw+2*p)/s + 1)^2 * kcount

	inputTransformed    *mat.Dense
	preDeltaTransformed *mat.Dense
	output              *mat.Dense
	activation          *mat.Dense
	delta               *mat.Dense

	weights, gradients *mat.Dense

	weightOptimizer Optimizer
	optGen          OptimizerInitializer

	activationFunction ActivationFunction
	name               string
}

func NewConvolutionalLayer(batchSize, inDepth, inWidth, kernCount,
	kernWidth, stride, pad int,
	activationFunction ActivationFunction) *ConvolutionalLayer {

	fmWidth := (inWidth-kernWidth+2*pad)/stride + 1

	inSize := batchSize * inDepth * inWidth * inWidth
	transSize := batchSize * fmWidth * fmWidth * kernWidth * kernWidth * inDepth
	outSize := batchSize * fmWidth * fmWidth * kernCount

	return &ConvolutionalLayer{
		batchSize:   batchSize,
		inputDepth:  inDepth,
		inputWidth:  inWidth,
		kernelCount: kernCount,
		kernelWidth: kernWidth,
		stride:      stride,
		pad:         pad,

		rawInput:               make([]float64, inSize),
		rawPreDelta:            make([]float64, inSize),
		rawInputTransformed:    make([]float64, transSize),
		rawPreDeltaTransformed: make([]float64, transSize),
		rawOutput:              make([]float64, outSize),
		rawActivation:          make([]float64, outSize),
		rawDelta:               make([]float64, outSize),

		activationFunction: activationFunction,
	}
}

func (layer *ConvolutionalLayer) Initialize(init Initializer) {

	fmWidth := (layer.inputWidth - layer.kernelWidth + 2*layer.pad)
	fmWidth /= layer.stride
	fmWidth++

	layer.inputTransformed = mat.NewDense(layer.batchSize*fmWidth*fmWidth,
		layer.inputDepth*layer.kernelWidth*layer.kernelWidth,
		layer.rawInputTransformed)

	layer.preDeltaTransformed = mat.NewDense(layer.batchSize*fmWidth*fmWidth,
		layer.inputDepth*layer.kernelWidth*layer.kernelWidth,
		layer.rawPreDeltaTransformed)

	layer.output = mat.NewDense(layer.batchSize*fmWidth*fmWidth,
		layer.kernelCount, layer.rawOutput)

	layer.activation = mat.NewDense(layer.batchSize*fmWidth*fmWidth,
		layer.kernelCount, layer.rawActivation)

	layer.delta = mat.NewDense(layer.batchSize*fmWidth*fmWidth,
		layer.kernelCount, layer.rawDelta)

	kLen := layer.kernelWidth * layer.kernelWidth * layer.inputDepth

	layer.weights = mat.NewDense(kLen, layer.kernelCount, nil)
	layer.gradients = mat.NewDense(kLen, layer.kernelCount, nil)

	layer.weightOptimizer = layer.optGen(layer.weights, layer.gradients)

	init.Initialize(layer.weights)
}

func (layer *ConvolutionalLayer) Forward() {
	Im2Row(layer.rawInput, layer.rawInputTransformed, layer.batchSize,
		layer.inputDepth, layer.inputWidth, layer.kernelWidth, layer.stride,
		layer.pad)

	layer.output.Mul(layer.inputTransformed, layer.weights)
	for i, net := range layer.rawOutput {
		layer.rawActivation[i] = layer.activationFunction.F(net)
	}
}

func (layer *ConvolutionalLayer) Backpropagate() {
	for i := range layer.rawOutput {
		layer.rawOutput[i] = layer.activationFunction.D(layer.rawOutput[i])
	}
	layer.delta.MulElem(layer.delta, layer.output)
	layer.preDeltaTransformed.Mul(layer.delta, layer.weights.T())
	memSetFloat64(layer.rawPreDelta, 0)
	Row2Im(layer.rawPreDelta, layer.rawPreDeltaTransformed, layer.batchSize,
		layer.inputDepth, layer.inputWidth, layer.kernelWidth, layer.stride,
		layer.pad)
}

func (layer *ConvolutionalLayer) CalculateGradients() {
	layer.gradients.Mul(layer.inputTransformed.T(), layer.delta)
}

func (layer *ConvolutionalLayer) Update(learningRate float64) {
	layer.weightOptimizer.Update(learningRate)
}

func (layer *ConvolutionalLayer) SetDelta(rawDelta []float64) {
	layer.rawDelta = rawDelta
}

func (layer *ConvolutionalLayer) SetInput(rawInput []float64) {
	layer.rawInput = rawInput
}

func (layer *ConvolutionalLayer) SetOptimizerInitializer(opt OptimizerInitializer) {
	layer.optGen = opt
}

func (layer *ConvolutionalLayer) Delta() []float64 {
	return layer.rawDelta
}

func (layer *ConvolutionalLayer) PreDelta() []float64 {
	return layer.rawPreDelta
}

func (layer *ConvolutionalLayer) Input() []float64 {
	return layer.rawInput
}

func (layer *ConvolutionalLayer) Activation() []float64 {
	return layer.rawActivation
}

func (layer *ConvolutionalLayer) WithName(name string) *ConvolutionalLayer {
	layer.name = name
	return layer
}

func (layer *ConvolutionalLayer) Name(name ...string) string {
	oldName := layer.name
	if name != nil {
		layer.name = name[0]
	}
	return oldName
}

func (layer *ConvolutionalLayer) Persist() error {
	if layer.name == "" {
		return fmt.Errorf("layer has no name")
	}

	f, err := os.Create(layer.name + ".dat.gz")
	if err != nil {
		return fmt.Errorf("unable to persist layer: %w", err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	defer w.Flush()

	gzw := gzip.NewWriter(w)
	defer gzw.Close()

	return binary.Write(gzw, binary.BigEndian, layer.weights.RawMatrix().Data)
}

func (layer *ConvolutionalLayer) Load() error {
	if layer.name == "" {
		return fmt.Errorf("layer has no name")
	}

	f, err := os.Open(layer.name + ".dat.gz")
	if err != nil {
		return fmt.Errorf("unable to load layer: %w", err)
	}
	defer f.Close()

	r := bufio.NewReader(f)

	gzr, err := gzip.NewReader(r)
	if err != nil {
		return fmt.Errorf("unable to load layer: %w", err)
	}
	defer gzr.Close()

	return binary.Read(gzr, binary.BigEndian, layer.weights.RawMatrix().Data)
}
