package convnet

import (
	"gitlab.com/wbrc/matdraw"
	"gonum.org/v1/gonum/mat"
)

type DenseLayer struct {
	batchSize, fanIn, fanOut int

	rawInput []float64
	input    *mat.Dense // batch*neurons_in

	rawOutput []float64
	output    *mat.Dense // batch*neurons_out

	rawActivation []float64
	activation    *mat.Dense // batch*neurons_out

	rawDelta []float64
	delta    *mat.Dense // batch*neurons_out

	rawPreDelta []float64
	preDelta    *mat.Dense // batch*neurons_in

	rawWeights []float64
	weights    *mat.Dense // neurons_in*neurons_out

	rawWeightsGradient []float64
	weightsGradient    *mat.Dense // neurons_in*neurons_out

	rawBias          []float64
	biasUpdateHelper *mat.Dense // 1*neurons_out
	bias             *mat.Dense // batch*neurons_out

	rawBiasGradient []float64
	biasGradient    *mat.Dense // 1*neurons_out

	actF            ActivationFunction
	init            Initializer
	optGen          OptimizerInitializer
	weightOptimizer Optimizer
	biasOptimizer   Optimizer
}

func NewDenseLayer(batchSize, fanIn, fanOut int,
	activationFunction ActivationFunction) *DenseLayer {

	rawInput := make([]float64, batchSize*fanIn)
	rawOutput := make([]float64, batchSize*fanOut)
	rawActivation := make([]float64, batchSize*fanOut)
	rawDelta := make([]float64, batchSize*fanOut)
	rawPreDelta := make([]float64, batchSize*fanIn)
	rawWeights := make([]float64, fanIn*fanOut)
	rawWeightsGradient := make([]float64, fanIn*fanOut)
	rawBias := make([]float64, fanOut)
	rawBiasGradient := make([]float64, fanOut)
	return &DenseLayer{
		batchSize: batchSize,
		fanIn:     fanIn,
		fanOut:    fanOut,

		rawInput:           rawInput,
		rawActivation:      rawActivation,
		rawDelta:           rawDelta,
		rawPreDelta:        rawPreDelta,
		rawOutput:          rawOutput,
		rawWeights:         rawWeights,
		rawWeightsGradient: rawWeightsGradient,
		rawBias:            rawBias,
		rawBiasGradient:    rawBiasGradient,

		actF: activationFunction,
	}

}

func (layer *DenseLayer) Initialize(init Initializer) {
	layer.input = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawInput)
	layer.output = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawOutput)
	layer.activation = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawActivation)
	layer.delta = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawDelta)
	layer.preDelta = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawPreDelta)
	layer.weights = mat.NewDense(layer.fanIn, layer.fanOut, layer.rawWeights)
	layer.weightsGradient = mat.NewDense(layer.fanIn, layer.fanOut, layer.rawWeightsGradient)
	layer.bias = mat.NewDense(layer.batchSize, layer.fanOut, nil)
	layer.biasUpdateHelper = mat.NewDense(1, layer.fanOut, layer.rawBias)
	layer.biasGradient = mat.NewDense(1, layer.fanOut, layer.rawBiasGradient)

	layer.weightOptimizer = layer.optGen(layer.weights, layer.weightsGradient)
	layer.biasOptimizer = layer.optGen(layer.biasUpdateHelper, layer.biasUpdateHelper)

	init.Initialize(layer.weights)
	init.Initialize(layer.biasUpdateHelper)
}

func (layer *DenseLayer) Forward() {

	layer.output.Mul(layer.input, layer.weights)
	populateBiasMatrix(layer.bias, layer.rawBias)
	layer.output.Add(layer.output, layer.bias)
	for i := range layer.rawOutput {
		layer.rawActivation[i] = layer.actF.F(layer.rawOutput[i])
	}
}

func (layer *DenseLayer) Backpropagate() {
	for i := range layer.rawOutput {
		layer.rawOutput[i] = layer.actF.D(layer.rawOutput[i])
	}
	layer.delta.MulElem(layer.delta, layer.output)
	layer.preDelta.Mul(layer.delta, layer.weights.T())
}

func (layer *DenseLayer) CalculateGradients() {
	layer.weightsGradient.Mul(layer.input.T(), layer.delta)
	r, _ := layer.bias.Dims()
	layer.biasGradient.CloneFrom(layer.delta.RowView(0).T())
	for i := 1; i < r; i++ {
		layer.biasGradient.Add(layer.biasGradient, layer.delta.RowView(i).T())
	}
}

func (layer *DenseLayer) Update(learningRate float64) {
	layer.weightOptimizer.Update(learningRate)
	layer.biasOptimizer.Update(learningRate)
}

func (layer *DenseLayer) SetDelta(rawDelta []float64) {
	layer.rawDelta = rawDelta
}

func (layer *DenseLayer) SetInput(rawInput []float64) {
	layer.rawInput = rawInput
}

func (layer *DenseLayer) PreDelta() []float64 {
	return layer.rawPreDelta
}

func (layer *DenseLayer) Activation() []float64 {
	return layer.rawActivation
}

func (layer *DenseLayer) Delta() []float64 {
	return layer.rawDelta
}

func (layer *DenseLayer) Input() []float64 {
	return layer.rawInput
}

func (layer *DenseLayer) SetOptimizerInitializer(opt OptimizerInitializer) {
	layer.optGen = opt
}

func (layer *DenseLayer) Draw(filename string) {
	matdraw.DrawRaw(layer.weights, filename+"-dense-weights.png")
	matdraw.DrawRaw(layer.activation, filename+"-dense-activation.png")
}

func populateBiasMatrix(m *mat.Dense, data []float64) {
	r, _ := m.Dims()
	for i := 0; i < r; i++ {
		copy(m.RawRowView(i), data)
	}
}
