package convnet

import "testing"

func TestMaxpoolLayer(t *testing.T) {

	input := []float64{
		1, 1,
		2, 1,
		1, 2,
		3, 2,
		2, 3,
		2, 2,
		4, 1,
		1, 4,
		3, 1,
		3, 4,
		2, 4,
		3, 2,
		2, 3,
		3, 6,
		4, 2,
		1, 2,
		2, 2,
		1, 1,
	}

	actualOutput := []float64{
		3, 3,
		2, 3,
		4, 4,
		3, 4,
		3, 6,
		4, 6,
		3, 6,
		4, 6,
	}

	delta := []float64{
		1, 1,
		-1, 1,
		1, -1,
		-1, -1,
		1, -1,
		-1, -1,
		1, -1,
		-1, -1,
	}

	actualPreDelta := []float64{
		0, 0,
		-1, 0,
		0, 0,
		1, 0,
		0, 2,
		0, 0,
		1, 0,
		0, -2,
		-1, 0,
		1, 0,
		0, 0,
		0, 0,
		0, 0,
		1, -4,
		-2, 0,
		0, 0,
		0, 0,
		0, 0,
	}

	l := NewMaxpoolLayer(2, 2, 3, 2, 1)
	l.SetInput(input)
	l.SetDelta(delta)

	l.Forward()
	for i := range l.rawOutput {
		if l.rawOutput[i] != actualOutput[i] {
			t.FailNow()
		}
	}

	l.Backpropagate()
	for i := range l.rawPreDelta {
		if l.rawPreDelta[i] != actualPreDelta[i] {
			t.FailNow()
		}
	}

}
