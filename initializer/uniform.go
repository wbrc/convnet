package initializer

import (
	"math/rand"

	"gitlab.com/wbrc/convnet"
	"gonum.org/v1/gonum/mat"
)

func UniformInitializer(lo, hi float64) convnet.Initializer {
	return convnet.InitializerFunc(func(m *mat.Dense) {
		for i := range m.RawMatrix().Data {
			m.RawMatrix().Data[i] = rand.Float64()*(hi-lo) + lo
		}
	})
}
