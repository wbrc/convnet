package initializer

import (
	"math/rand"

	"gitlab.com/wbrc/convnet"
	"gonum.org/v1/gonum/mat"
)

func NormalInitializer(mean, stdDeviation float64) convnet.Initializer {
	return convnet.InitializerFunc(func(m *mat.Dense) {
		for i := range m.RawMatrix().Data {
			m.RawMatrix().Data[i] = rand.NormFloat64()*stdDeviation + mean
		}
	})
}
