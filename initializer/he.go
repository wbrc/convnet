package initializer

import (
	"math"
	"math/rand"

	"gitlab.com/wbrc/convnet"
	"gonum.org/v1/gonum/mat"
)

func HeInitializer() convnet.Initializer {
	return convnet.InitializerFunc(func(m *mat.Dense) {

		rows, cols := m.Dims()

		for i := range m.RawMatrix().Data {
			m.RawMatrix().Data[i] = math.Sqrt(2/float64(rows+cols)) * rand.NormFloat64()
		}
	})
}
