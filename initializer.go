package convnet

import "gonum.org/v1/gonum/mat"

type Initializer interface {
	Initialize(m *mat.Dense)
}

type InitializerFunc func(m *mat.Dense)

func (initializerFunction InitializerFunc) Initialize(m *mat.Dense) {
	initializerFunction(m)
}
