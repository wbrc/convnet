package convnet

import (
	"math"
)

type MaxpoolLayer struct {
	batchSize, inputDepth, outWidth int
	inputWidth, poolSize, stride    int

	rawInput    []float64
	rawPreDelta []float64
	rawOutput   []float64
	rawDelta    []float64
	maxMask     []byte
}

func NewMaxpoolLayer(batchSize, inDepth, inWidth, poolSize, stride int) *MaxpoolLayer {

	outWidth := (inWidth-poolSize)/stride + 1

	inSize := batchSize * inDepth * inWidth * inWidth
	outSize := batchSize * inDepth * outWidth * outWidth

	return &MaxpoolLayer{
		batchSize:  batchSize,
		inputDepth: inDepth,
		inputWidth: inWidth,
		outWidth:   outWidth,
		poolSize:   poolSize,
		stride:     stride,

		rawInput:    make([]float64, inSize),
		rawPreDelta: make([]float64, inSize),
		rawOutput:   make([]float64, outSize),
		rawDelta:    make([]float64, outSize),
		maxMask:     make([]byte, outSize),
	}
}

func (layer *MaxpoolLayer) Initialize(init Initializer) {}

func (layer *MaxpoolLayer) Forward() {
	for batch := 0; batch < layer.batchSize; batch++ {
		for fm := 0; fm < layer.inputDepth; fm++ {
			for row := 0; row < layer.inputWidth-layer.poolSize+1; row += layer.stride {
				for col := 0; col < layer.inputWidth-layer.poolSize+1; col += layer.stride {

					max := -math.MaxFloat64
					mr, mc := 0, 0
					for wrow := 0; wrow < layer.poolSize; wrow++ {
						for wcol := 0; wcol < layer.poolSize; wcol++ {
							cell := InputGetPixel(layer.rawInput, layer.inputDepth, layer.inputWidth, 0, batch, fm, row+wrow, col+wcol)
							if max < cell {
								max = cell
								mr, mc = wrow, wcol
							}
						}
					}
					oindex := batch*layer.outWidth*layer.outWidth*layer.inputDepth + ((row/layer.stride)*layer.outWidth+(col/layer.stride))*layer.inputDepth + fm
					layer.rawOutput[oindex] = max
					layer.maxMask[oindex] = encodePos(mr, mc)
				}
			}
		}
	}
}

func (layer *MaxpoolLayer) Backpropagate() {
	memSetFloat64(layer.rawPreDelta, 0)

	for batch := 0; batch < layer.batchSize; batch++ {
		for fm := 0; fm < layer.inputDepth; fm++ {
			for row := 0; row < layer.inputWidth-layer.poolSize+1; row += layer.stride {
				for col := 0; col < layer.inputWidth-layer.poolSize+1; col += layer.stride {
					oindex := batch*layer.outWidth*layer.outWidth*layer.inputDepth + ((row/layer.stride)*layer.outWidth+(col/layer.stride))*layer.inputDepth + fm
					mr, mc := decodePos(layer.maxMask[oindex])
					InputAddPixel(layer.rawPreDelta, layer.rawDelta[oindex], layer.inputDepth, layer.inputWidth, 0, batch, fm, row+mr, col+mc)
				}
			}
		}
	}

}

func (layer *MaxpoolLayer) CalculateGradients() {}

func (layer *MaxpoolLayer) Update(learningRate float64) {}

func (layer *MaxpoolLayer) SetDelta(rawDelta []float64) {
	layer.rawDelta = rawDelta
}

func (layer *MaxpoolLayer) SetInput(rawInput []float64) {
	layer.rawInput = rawInput
}

func (layer *MaxpoolLayer) SetOptimizerInitializer(opt OptimizerInitializer) {}

func (layer *MaxpoolLayer) Delta() []float64 {
	return layer.rawDelta
}

func (layer *MaxpoolLayer) PreDelta() []float64 {
	return layer.rawPreDelta
}

func (layer *MaxpoolLayer) Input() []float64 {
	return layer.rawInput
}

func (layer *MaxpoolLayer) Activation() []float64 {
	return layer.rawOutput
}

func encodePos(row, col int) byte {
	return byte(row)<<4 | byte(col)&0x0f
}

func decodePos(code byte) (row, col int) {
	row = int(code >> 4)
	col = int(code & 0x0f)
	return
}
