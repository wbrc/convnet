package convnet

import (
	"math/rand"

	"gonum.org/v1/gonum/mat"
)

type DropoutLayer struct {
	batchSize, fanIn int
	dropout          float64
	trainMode        bool

	rawInput []float64
	input    *mat.Dense

	rawMask []float64
	mask    *mat.Dense

	rawActivation []float64
	activation    *mat.Dense

	rawDelta []float64
	delta    *mat.Dense

	rawPreDelta []float64
	preDelta    *mat.Dense
}

func NewDropoutLayer(batchSize, fanIn int, dropout float64) *DropoutLayer {

	rawInput := make([]float64, batchSize*fanIn)
	rawMask := make([]float64, batchSize*fanIn)
	rawActivation := make([]float64, batchSize*fanIn)
	rawDelta := make([]float64, batchSize*fanIn)
	rawPreDelta := make([]float64, batchSize*fanIn)
	return &DropoutLayer{
		batchSize: batchSize,
		fanIn:     fanIn,
		dropout:   dropout,
		trainMode: true,

		rawInput:      rawInput,
		rawMask:       rawMask,
		rawActivation: rawActivation,
		rawDelta:      rawDelta,
		rawPreDelta:   rawPreDelta,
	}

}

func (layer *DropoutLayer) Initialize(init Initializer) {
	layer.input = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawInput)
	layer.mask = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawMask)
	layer.activation = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawActivation)
	layer.delta = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawDelta)
	layer.preDelta = mat.NewDense(layer.batchSize, layer.fanIn, layer.rawPreDelta)
}

func (layer *DropoutLayer) Forward() {
	if layer.trainMode {
		for i := range layer.rawMask {
			if rand.Float64() > layer.dropout {
				layer.rawMask[i] = 1
			} else {
				layer.rawMask[i] = 0
			}
		}
	} else {
		memSetFloat64(layer.rawMask, 1-layer.dropout)
	}

	layer.activation.MulElem(layer.input, layer.mask)
}

func (layer *DropoutLayer) Backpropagate() {
	layer.preDelta.MulElem(layer.delta, layer.mask)
}

func (layer *DropoutLayer) CalculateGradients() {}

func (layer *DropoutLayer) Update(learningRate float64) {}

func (layer *DropoutLayer) SetDelta(rawDelta []float64) {
	layer.rawDelta = rawDelta
}

func (layer *DropoutLayer) SetInput(rawInput []float64) {
	layer.rawInput = rawInput
}

func (layer *DropoutLayer) PreDelta() []float64 {
	return layer.rawPreDelta
}

func (layer *DropoutLayer) Activation() []float64 {
	return layer.rawActivation
}

func (layer *DropoutLayer) Delta() []float64 {
	return layer.rawDelta
}

func (layer *DropoutLayer) Input() []float64 {
	return layer.rawInput
}

func (layer *DropoutLayer) SetOptimizerInitializer(opt OptimizerInitializer) {}

func (layer *DropoutLayer) EnableTrainMode() {
	layer.trainMode = true
}
func (layer *DropoutLayer) EnablePredictMode() {
	layer.trainMode = false
}
