package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"gitlab.com/wbrc/progress"
)

const (
	dsURL   = "https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz"
	bufSize = 4096
)

func loadDataset() (string, error) {

	dirs, err := ioutil.ReadDir("/tmp")
	if err != nil {
		return "", err
	}
	for _, d := range dirs {
		if d.IsDir() && strings.Contains(d.Name(), "cifar_") {
			return "/tmp/" + d.Name() + "/" + getName(dsURL), nil
		}
	}

	tmpDir, err := ioutil.TempDir("", "cifar_")
	if err != nil {
		return "", err
	}

	if err := fetchAndStore(dsURL, tmpDir+"/"+getName(dsURL)); err != nil {
		return "", err
	}
	return tmpDir + "/" + getName(dsURL), nil
}

func fetchAndStore(url, to string) error {
	file, err := os.Create(to)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	defer w.Flush()
	return fetch(url, w)
}

func fetch(url string, w io.Writer) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Printf("Fetch %s ", getName(url))
	bar := progress.Bar(int(resp.ContentLength))
	buf := make([]byte, bufSize)

	for n, err := resp.Body.Read(buf); err == nil || n > 0; n, err = resp.Body.Read(buf) {
		w.Write(buf[:n])
		bar(n)
	}
	fmt.Println()
	return nil
}

func getName(url string) string {
	tkns := strings.Split(url, "/")
	return tkns[len(tkns)-1]
}
