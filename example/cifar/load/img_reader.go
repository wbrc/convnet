package load

import (
	"archive/tar"
	"bufio"
	"bytes"
	"compress/gzip"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type Image [][][]float64

type Dataset struct {
	images []Image
	labels []byte
}

func (ds *Dataset) Count() int {
	return len(ds.images)
}

func (ds *Dataset) GetFloat(i int) ([][][]float64, byte) {
	return ds.images[i], ds.labels[i]
}

func LoadDataset(fileName string) (*Dataset, *Dataset, error) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Print("open err")
		return nil, nil, err
	}
	defer file.Close()
	bufin := bufio.NewReader(file)
	zipreader, err := gzip.NewReader(bufin)
	if err != nil {
		log.Print("gzip err")
		return nil, nil, err
	}
	defer zipreader.Close()
	tarfile := tar.NewReader(zipreader)

	ds := Dataset{
		make([]Image, 0, 50000),
		make([]byte, 0, 50000),
	}

	ts := Dataset{
		make([]Image, 0, 10000),
		make([]byte, 0, 10000),
	}

	for h, err := tarfile.Next(); err == nil; h, err = tarfile.Next() {
		if strings.Contains(h.Name, "data_") {
			buf, err := ioutil.ReadAll(tarfile)
			if err != nil {
				return nil, nil, err
			}
			buffer := bytes.NewBuffer(buf)
			for i := 0; i < 10000; i++ {
				label, err := buffer.ReadByte()
				if err != nil {
					return nil, nil, err
				}
				ds.labels = append(ds.labels, label)
				imgarr := make([]byte, 3072)
				n, err := buffer.Read(imgarr)
				if err != nil || n != 3072 {
					return nil, nil, fmt.Errorf("fuck")
				}
				img := newimg()
				for i := range imgarr {
					z := i / (32 * 32)
					ii := i - z*32*32
					x := ii % 32
					y := ii / 32
					img[z][x][y] = float64(imgarr[i]) / 255.0
				}
				ds.images = append(ds.images, img)
			}
		} else if strings.Contains(h.Name, "test_") {
			buf, err := ioutil.ReadAll(tarfile)
			if err != nil {
				return nil, nil, err
			}
			buffer := bytes.NewBuffer(buf)
			for i := 0; i < 10000; i++ {
				label, err := buffer.ReadByte()
				if err != nil {
					return nil, nil, err
				}
				ts.labels = append(ts.labels, label)
				imgarr := make([]byte, 3072)
				n, err := buffer.Read(imgarr)
				if err != nil || n != 3072 {
					return nil, nil, fmt.Errorf("fuck")
				}
				img := newimg()
				for i := range imgarr {
					z := i / (32 * 32)
					ii := i - z*32*32
					x := ii % 32
					y := ii / 32
					img[z][x][y] = float64(imgarr[i]) / 255.0
				}
				ts.images = append(ts.images, img)
			}
		}
	}

	return &ds, &ts, nil
}

func newimg() (img Image) {
	img = make([][][]float64, 3)
	for d := range img {
		img[d] = make([][]float64, 32)
		for x := range img[d] {
			img[d][x] = make([]float64, 32)
		}
	}
	return
}
