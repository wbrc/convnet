package main

import (
	"log"

	"gitlab.com/wbrc/convnet/activation"

	"gitlab.com/wbrc/convnet"
	cifards "gitlab.com/wbrc/convnet/example/cifar/load"
	"gitlab.com/wbrc/convnet/initializer"
	"gitlab.com/wbrc/convnet/optimizer"
)

func main() {
	batchSize := 50

	trainData, testData := readData(batchSize)

	net := &convnet.Network{
		GlobalInitializer:          initializer.HeInitializer(),
		GlobalOptimizerInitializer: optimizer.RmspropInitializer(0.99),
	}

	net.Add(convnet.NewPreparationLayer(batchSize, 3, 32))

	net.Add(convnet.NewConvolutionalLayer(batchSize, 3, 32, 32, 3, 1, 1, activation.SReLU{Tr: 7, Tl: 0, Ar: 0.2, Al: 0.001}).WithName("conv1"))
	net.Add(convnet.NewConvolutionalLayer(batchSize, 32, 32, 32, 3, 1, 1, activation.SReLU{Tr: 7, Tl: 0, Ar: 0.2, Al: 0.001}).WithName("conv2"))
	net.Add(convnet.NewMaxpoolLayer(batchSize, 32, 32, 2, 2))
	net.Add(convnet.NewDropoutLayer(batchSize, 32*16*16, 0.1))

	net.Add(convnet.NewConvolutionalLayer(batchSize, 32, 16, 64, 3, 1, 1, activation.SReLU{Tr: 5, Tl: 0, Ar: 0.1, Al: 0.01}).WithName("conv3"))
	net.Add(convnet.NewConvolutionalLayer(batchSize, 64, 16, 64, 3, 1, 1, activation.SReLU{Tr: 5, Tl: 0, Ar: 0.1, Al: 0.01}).WithName("conv4"))
	net.Add(convnet.NewMaxpoolLayer(batchSize, 64, 16, 2, 2))
	net.Add(convnet.NewDropoutLayer(batchSize, 64*8*8, 0.1))

	net.Add(convnet.NewConvolutionalLayer(batchSize, 64, 8, 128, 3, 1, 1, activation.SReLU{Tr: 5, Tl: 0, Ar: 0.1, Al: 0.01}).WithName("conv5"))
	net.Add(convnet.NewConvolutionalLayer(batchSize, 128, 8, 128, 3, 1, 1, activation.SReLU{Tr: 5, Tl: 0, Ar: 0.1, Al: 0.01}).WithName("conv6"))
	net.Add(convnet.NewMaxpoolLayer(batchSize, 128, 8, 2, 2))
	net.Add(convnet.NewDropoutLayer(batchSize, 128*4*4, 0.1))

	net.Add(convnet.NewDenseLayer(batchSize, 128*4*4, 512, activation.SReLU{Tr: 3, Tl: -0.1, Ar: 0.3, Al: 0.001}))
	net.Add(convnet.NewDropoutLayer(batchSize, 512, 0.25))
	net.Add(convnet.NewDenseLayer(batchSize, 512, 64, activation.SReLU{Tr: 3, Tl: -0.1, Ar: 0.3, Al: 0.001}))
	net.Add(convnet.NewDenseLayer(batchSize, 64, 10, activation.SReLU{Tr: 3, Tl: -0.1, Ar: 0.3, Al: 0.001}))
	net.Add(convnet.NewSoftmaxLayer(batchSize, 10))

	net.Initialize()

	net.Load()

	engine := &convnet.Engine{
		Epoch:         100,
		BatchCallback: func(n int, tr, te float64) { log.Printf("Batch %d: train error: %f test error: %f\n", n+1, tr, te) },
		EpochCallback: func(n int, err, acc float64) {
			log.Printf("Epoch %d: error: %f accuracy: %f\n", n+1, err, acc)
			net.Persist()
		},
		LrSchedule: func(n int) float64 {
			if n < 5 {
				return 0.00004
			}
			if n < 10 {
				return 0.00008
			}
			if n < 50 {
				return 0.0001
			}
			return 0.00005
		},
		Model:     net,
		TestData:  testData,
		TrainData: trainData,
	}

	engine.Start()

}

func readData(batchSize int) (*convnet.Dataset, *convnet.Dataset) {
	fileName, err := loadDataset()
	if err != nil {
		log.Fatal(err)
	}

	train, test, err := cifards.LoadDataset(fileName)
	if err != nil {
		log.Fatal(err)
	}

	trainDataset := &convnet.Dataset{BatchSize: batchSize}
	testDataset := &convnet.Dataset{BatchSize: batchSize}

	for i := 0; i < train.Count(); i++ {
		rawInput, rawLabel := train.GetFloat(i)
		rawImg, rawOut := make([]float64, 0, 3*32*32), make([]float64, 10)
		for d := range rawInput {
			for r := range rawInput[d] {
				rawImg = append(rawImg, rawInput[d][r]...)
			}
		}
		rawOut[rawLabel] = 1
		trainDataset.Add(rawImg, rawOut)
	}

	for i := 0; i < test.Count(); i++ {
		rawInput, rawLabel := test.GetFloat(i)
		rawImg, rawOut := make([]float64, 0, 3*32*32), make([]float64, 10)
		for d := range rawInput {
			for r := range rawInput[d] {
				rawImg = append(rawImg, rawInput[d][r]...)
			}
		}
		rawOut[rawLabel] = 1
		testDataset.Add(rawImg, rawOut)
	}

	return trainDataset, testDataset
}
