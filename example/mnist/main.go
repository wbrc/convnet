package main

import (
	"log"

	"gitlab.com/wbrc/convnet/activation"

	"gitlab.com/wbrc/convnet"
	"gitlab.com/wbrc/convnet/initializer"
	"gitlab.com/wbrc/convnet/optimizer"

	mnist "github.com/petar/GoMNIST"
)

func main() {
	batchSize := 100

	trainData, testData := readData(batchSize)

	net := &convnet.Network{
		GlobalInitializer:          initializer.HeInitializer(),
		GlobalOptimizerInitializer: optimizer.RmspropInitializer(0.99),
	}

	net.Add(convnet.NewPreparationLayer(batchSize, 1, 28))

	net.Add(convnet.NewConvolutionalLayer(batchSize, 1, 28, 16, 5, 1, 2, activation.LeakyReLU{Alpha: 0.1}))
	net.Add(convnet.NewConvolutionalLayer(batchSize, 16, 28, 16, 5, 1, 2, activation.LeakyReLU{Alpha: 0.1}))
	net.Add(convnet.NewMaxpoolLayer(batchSize, 16, 28, 2, 2))

	net.Add(convnet.NewConvolutionalLayer(batchSize, 16, 14, 32, 3, 1, 1, activation.LeakyReLU{Alpha: 0.1}))
	net.Add(convnet.NewConvolutionalLayer(batchSize, 32, 14, 32, 3, 1, 1, activation.LeakyReLU{Alpha: 0.1}))
	net.Add(convnet.NewMaxpoolLayer(batchSize, 32, 14, 2, 2))

	net.Add(convnet.NewDenseLayer(batchSize, 32*7*7, 512, activation.LeakyReLU{Alpha: 0.1}))
	net.Add(convnet.NewDropoutLayer(batchSize, 512, 0.3))
	net.Add(convnet.NewDenseLayer(batchSize, 512, 10, activation.LeakyReLU{Alpha: 0.1}))
	net.Add(convnet.NewSoftmaxLayer(batchSize, 10))

	net.Initialize()

	engine := &convnet.Engine{
		Epoch:         100,
		BatchCallback: func(n int, tr, te float64) { log.Printf("Batch %d: train error: %f test error: %f\n", n+1, tr, te) },
		EpochCallback: func(n int, err, acc float64) { log.Printf("Epoch %d: error: %f accuracy: %f\n", n+1, err, acc) },
		LrSchedule: func(n int) float64 {
			if n == 0 {
				return 0.001
			} else if n <= 3 {
				return 0.002
			} else if n <= 25 {
				return 0.0006
			}
			return 0.0002
		},
		Model:     net,
		TestData:  testData,
		TrainData: trainData,
	}

	engine.Start()

}

func readData(batchSize int) (*convnet.Dataset, *convnet.Dataset) {
	dir, err := loadDataset()
	if err != nil {
		log.Fatal(err)
	}

	train, test, err := mnist.Load(dir)
	if err != nil {
		log.Fatal(err)
	}

	trainDataset := &convnet.Dataset{BatchSize: batchSize}
	testDataset := &convnet.Dataset{BatchSize: batchSize}

	for i := 0; i < train.Count(); i++ {
		byteImg, byteLabel := train.Get(i)
		rawImg, rawOut := make([]float64, 28*28), make([]float64, 10)
		for i, b := range byteImg {
			rawImg[i] = float64(b) / 255.0
		}
		rawOut[byteLabel] = 1
		trainDataset.Add(rawImg, rawOut)
	}

	for i := 0; i < test.Count(); i++ {
		byteImg, byteLabel := test.Get(i)
		rawImg, rawOut := make([]float64, 28*28), make([]float64, 10)
		for i, b := range byteImg {
			rawImg[i] = float64(b) / 255.0
		}
		rawOut[byteLabel] = 1
		testDataset.Add(rawImg, rawOut)
	}

	return trainDataset, testDataset
}
