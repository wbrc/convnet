package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"gitlab.com/wbrc/progress"
)

const (
	trainImagesURL = "http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz"
	trainLabelsURL = "http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz"
	testImagesURL  = "http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz"
	testLabelsURL  = "http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz"
	bufSize        = 4096
)

func loadDataset() (string, error) {

	dirs, err := ioutil.ReadDir("/tmp")
	if err != nil {
		return "", err
	}
	for _, d := range dirs {
		if d.IsDir() && strings.Contains(d.Name(), "mnist_") {
			return "/tmp/" + d.Name(), nil
		}
	}

	tmpDir, err := ioutil.TempDir("", "mnist_")
	if err != nil {
		return "", err
	}

	if err := fetchAndStore(trainImagesURL, tmpDir+"/"+getName(trainImagesURL)); err != nil {
		return "", err
	}

	if err := fetchAndStore(trainLabelsURL, tmpDir+"/"+getName(trainLabelsURL)); err != nil {
		return "", err
	}

	if err := fetchAndStore(testImagesURL, tmpDir+"/"+getName(testImagesURL)); err != nil {
		return "", err
	}

	if err := fetchAndStore(testLabelsURL, tmpDir+"/"+getName(testLabelsURL)); err != nil {
		return "", err
	}
	return tmpDir, nil
}

func fetchAndStore(url, to string) error {
	file, err := os.Create(to)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	defer w.Flush()
	return fetch(url, w)
}

func fetch(url string, w io.Writer) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Printf("Fetch %s ", getName(url))
	bar := progress.Bar(int(resp.ContentLength))
	buf := make([]byte, bufSize)

	for n, err := resp.Body.Read(buf); err == nil || n > 0; n, err = resp.Body.Read(buf) {
		w.Write(buf[:n])
		bar(n)
	}
	fmt.Println()
	return nil
}

func getName(url string) string {
	tkns := strings.Split(url, "/")
	return tkns[len(tkns)-1]
}
