package convnet

import (
	"fmt"

	"gonum.org/v1/gonum/mat"
)

func memSetFloat64(target []float64, val float64) {
	if len(target) == 0 {
		return
	}
	target[0] = val
	for bp := 1; bp < len(target); bp *= 2 {
		copy(target[bp:], target[:bp])
	}
}

func PrintMatrix(m mat.Matrix) {
	r, c := m.Dims()
	for ri := 0; ri < r; ri++ {
		for ci := 0; ci < c; ci++ {
			fmt.Printf("%3.2f ", m.At(ri, ci))
		}
		fmt.Println()
	}
	fmt.Println()
}
