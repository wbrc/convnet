package convnet

type Layer interface {
	Initialize(init Initializer)

	Forward()
	Backpropagate()
	CalculateGradients()
	Update(learningRate float64)

	SetDelta(rawDelta []float64)
	SetInput(rawInput []float64)
	SetOptimizerInitializer(opt OptimizerInitializer)

	Delta() []float64
	PreDelta() []float64
	Input() []float64
	Activation() []float64
}

type ErrorLayer interface {
	Layer
	Error() float64
}

type ModeToggler interface {
	EnableTrainMode()
	EnablePredictMode()
}

type Drawer interface {
	Draw(filename string)
}

type PersistableLayer interface {
	Name(...string) string
	Persist() error
	Load() error
}
