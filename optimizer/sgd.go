package optimizer

import (
	"gitlab.com/wbrc/convnet"
	"gonum.org/v1/gonum/mat"
)

func SgdInitializer(params, gradients *mat.Dense) convnet.Optimizer {
	pr, pc := params.Dims()
	gr, gc := gradients.Dims()
	if pr != gr || pc != gc {
		panic("params and gradients must be of same size")
	}
	return convnet.UpdateFunc(func(learningRate float64) {
		gradients.Scale(learningRate, gradients)
		params.Sub(params, gradients)
	})
}
