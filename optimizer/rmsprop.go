package optimizer

import (
	"math"

	"gitlab.com/wbrc/convnet"
	"gonum.org/v1/gonum/mat"
)

func RmspropInitializer(beta float64) convnet.OptimizerInitializer {
	return func(params, gradients *mat.Dense) convnet.Optimizer {
		pr, pc := params.Dims()
		gr, gc := gradients.Dims()
		if pr != gr || pc != gc {
			panic("params and gradients must be of same size")
		}

		cache := mat.NewDense(pr, pc, nil)
		sq := mat.NewDense(pr, pc, nil)

		return convnet.UpdateFunc(func(learningRate float64) {
			cache.Scale(beta, cache)
			sq.MulElem(gradients, gradients)
			sq.Scale(1-beta, sq)
			cache.Add(cache, sq)

			for i := range cache.RawMatrix().Data {
				sq.RawMatrix().Data[i] = (learningRate/(math.Sqrt(cache.RawMatrix().Data[i])) + 1e-8) * gradients.RawMatrix().Data[i]
			}

			params.Sub(params, sq)
		})
	}
}
