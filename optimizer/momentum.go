package optimizer

import (
	"gitlab.com/wbrc/convnet"
	"gonum.org/v1/gonum/mat"
)

func MomentumInitializer(momentum float64) convnet.OptimizerInitializer {
	return func(params, gradients *mat.Dense) convnet.Optimizer {
		pr, pc := params.Dims()
		gr, gc := gradients.Dims()
		if pr != gr || pc != gc {
			panic("params and gradients must be of same size")
		}

		cache := mat.NewDense(pr, pc, nil)

		return convnet.UpdateFunc(func(learningRate float64) {
			cache.Scale(momentum, cache)
			gradients.Scale((1-momentum)*learningRate, gradients)
			cache.Add(cache, gradients)
			params.Sub(params, cache)
		})
	}
}
