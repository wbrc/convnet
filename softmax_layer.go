package convnet

import (
	"math"

	"gonum.org/v1/gonum/mat"
)

type SoftmaxLayer struct {
	fanOut, batchSize int

	rawInput []float64
	input    *mat.Dense // batch*neurons_out

	rawActivation []float64
	activation    *mat.Dense // batch*neurons_out

	rawDelta []float64
	delta    *mat.Dense // batch*neurons_out

	rawPreDelta []float64
	preDelta    *mat.Dense // batch*neurons_out
}

func NewSoftmaxLayer(batchSize, fanOut int) *SoftmaxLayer {
	rawInput := make([]float64, batchSize*fanOut)
	rawActivation := make([]float64, batchSize*fanOut)
	rawDelta := make([]float64, batchSize*fanOut)
	rawPreDelta := make([]float64, batchSize*fanOut)

	return &SoftmaxLayer{
		batchSize: batchSize,
		fanOut:    fanOut,

		rawInput:      rawInput,
		rawActivation: rawActivation,
		rawDelta:      rawDelta,
		rawPreDelta:   rawPreDelta,
	}
}

func (layer *SoftmaxLayer) Initialize(init Initializer) {
	layer.input = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawInput)
	layer.activation = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawActivation)
	layer.delta = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawDelta)
	layer.preDelta = mat.NewDense(layer.batchSize, layer.fanOut, layer.rawPreDelta)
}

func (layer *SoftmaxLayer) Forward() {
	r, _ := layer.input.Dims()
	for i := 0; i < r; i++ {
		var sum float64
		for j := 0; j < layer.input.RowView(i).Len(); j++ {
			sum += math.Exp(layer.input.RowView(i).AtVec(j))
		}
		for j := 0; j < layer.activation.RowView(i).Len(); j++ {
			layer.activation.Set(i, j, math.Exp(layer.input.At(i, j))/sum)
		}
	}
}

func (layer *SoftmaxLayer) Backpropagate() {
	layer.preDelta.Sub(layer.activation, layer.delta)
}

func (layer *SoftmaxLayer) CalculateGradients()         {}
func (layer *SoftmaxLayer) Update(learningRate float64) {}

func (layer *SoftmaxLayer) Activation() []float64 {
	return layer.rawActivation
}

func (layer *SoftmaxLayer) Delta() []float64 {
	return layer.rawDelta
}

func (layer *SoftmaxLayer) Input() []float64 {
	return layer.rawInput
}

func (layer *SoftmaxLayer) PreDelta() []float64 {
	return layer.rawPreDelta
}

func (layer *SoftmaxLayer) SetDelta(delta []float64) {
	layer.rawDelta = delta
}

func (layer *SoftmaxLayer) SetInput(input []float64) {
	layer.rawInput = input
}

func (layer *SoftmaxLayer) SetOptimizerInitializer(opt OptimizerInitializer) {}

func (layer *SoftmaxLayer) Error() float64 {
	var err float64
	for i, d := range layer.rawDelta {
		err -= d * math.Log(layer.rawActivation[i])
	}
	return err / float64(layer.batchSize)
}

var _ ErrorLayer = NewSoftmaxLayer(32, 10)
