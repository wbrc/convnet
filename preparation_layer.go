package convnet

type PreparationLayer struct {
	batchSize, inputDepth, inputWidth int

	rawInput  []float64 // bs*indep*inwid*inwid
	rawOutput []float64 // bs*indep*inwid*inwid
}

func NewPreparationLayer(batchSize, inDepth, inWidth int) *PreparationLayer {

	return &PreparationLayer{
		batchSize:  batchSize,
		inputDepth: inDepth,
		inputWidth: inWidth,

		rawInput:  make([]float64, batchSize*inDepth*inWidth*inWidth),
		rawOutput: make([]float64, batchSize*inDepth*inWidth*inWidth),
	}
}

func (layer *PreparationLayer) Initialize(init Initializer) {}

func (layer *PreparationLayer) Forward() {
	TransformColview(layer.rawInput, layer.rawOutput, layer.batchSize,
		layer.inputDepth, layer.inputWidth)
}

func (layer *PreparationLayer) Backpropagate() {}

func (layer *PreparationLayer) CalculateGradients() {}

func (layer *PreparationLayer) Update(learningRate float64) {}

func (layer *PreparationLayer) SetDelta(rawDelta []float64) {}

func (layer *PreparationLayer) SetInput(rawInput []float64) {
	layer.rawInput = rawInput
}

func (layer *PreparationLayer) SetOptimizerInitializer(opt OptimizerInitializer) {}

func (layer *PreparationLayer) Delta() []float64 {
	// we wrap this panic in a func so go-vet ignores this
	func() { panic("this makes no sense. i'm intended as a first layer") }()
	return nil
}

func (layer *PreparationLayer) PreDelta() []float64 {
	// we wrap this panic in a func so go-vet ignores this
	func() { panic("this makes no sense. i'm intended as a first layer") }()
	return nil
}

func (layer *PreparationLayer) Input() []float64 {
	return layer.rawInput
}

func (layer *PreparationLayer) Activation() []float64 {
	return layer.rawOutput
}
