package convnet

import "gonum.org/v1/gonum/mat"

type Optimizer interface {
	Update(learningRate float64)
}

type OptimizerInitializer func(params, gradients *mat.Dense) Optimizer

type UpdateFunc func(learningRate float64)

func (uf UpdateFunc) Update(learningRate float64) {
	uf(learningRate)
}
