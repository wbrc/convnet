package activation

import (
	"math"
)

type Tanh struct{}

func (a Tanh) F(x float64) float64 {
	return math.Tanh(x)
}

func (a Tanh) D(x float64) float64 {
	return (1 + math.Tanh(x)) * (1 - math.Tanh(x))
}
