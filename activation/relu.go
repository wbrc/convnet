package activation

type ReLU struct{}

func (a ReLU) F(x float64) float64 {
	if x > 0 {
		return x
	}
	return 0
}

func (a ReLU) D(x float64) float64 {
	if x > 0 {
		return 1
	}
	return 0
}
