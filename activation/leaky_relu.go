package activation

type LeakyReLU struct {
	Alpha float64
}

func (a LeakyReLU) F(x float64) float64 {
	if x > 0 {
		return x
	}
	return a.Alpha * x
}

func (a LeakyReLU) D(x float64) float64 {
	if x > 0 {
		return 1
	}
	return a.Alpha
}
