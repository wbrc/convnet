package activation

import (
	"math"
)

type Sigmoid struct{}

func (a Sigmoid) F(x float64) float64 {
	return 1 / (1 + math.Exp(-x))
}

func (a Sigmoid) D(x float64) float64 {
	return a.F(x) * (1 - a.F(x))
}
