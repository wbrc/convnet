package activation

type SReLU struct {
	Tr, Tl float64
	Ar, Al float64
}

func (a SReLU) F(x float64) float64 {
	if x >= a.Tr {
		return a.Tr + a.Ar*(x-a.Tr)
	} else if x > a.Tl {
		return x
	}
	return a.Tl + a.Al*(x-a.Tl)

}

func (a SReLU) D(x float64) float64 {
	if x >= a.Tr {
		return a.Ar
	} else if x > a.Tl {
		return 1
	}
	return a.Al
}
