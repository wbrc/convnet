package activation

type Identity struct{}

func (a Identity) F(x float64) float64 {
	return x
}

func (a Identity) D(x float64) float64 {
	return 1
}
