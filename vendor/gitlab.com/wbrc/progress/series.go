package progress

import "fmt"

func Series(maxCount int) func() string {
	var count int
	formatString := makeFormatString(maxCount)
	return func() string {
		count++
		return fmt.Sprintf(formatString, count, maxCount)
	}
}

func makeFormatString(max int) string {
	width := len(fmt.Sprintf("%d", max))
	return fmt.Sprintf("%%%dd / %%%dd", width, width)
}
