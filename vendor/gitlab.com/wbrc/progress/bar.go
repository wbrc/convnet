package progress

import (
	"fmt"
	"os"
	"strings"

	goisatty "github.com/mattn/go-isatty"
)

const (
	ESC            = "\x1b["
	SAVE_CURSOR    = ESC + "s"
	RESTORE_CURSOR = ESC + "u"

	DEFAULT_BAR_WIDTH = 14
)

func Bar(maxCount int, width ...int) func(inc int) {
	var count, per int
	barwidth := DEFAULT_BAR_WIDTH
	if len(width) > 0 {
		barwidth = width[0]
	}

	sb := &strings.Builder{}
	sb.Grow(barwidth + 6)

	istty := goisatty.IsTerminal(os.Stdout.Fd())

	getBarString := func() string {
		sb.Reset()
		sb.WriteString(fmt.Sprintf("%3d%% [", per))
		remain := barwidth
		for i := 0; i < barwidth*per/100; i++ {
			sb.WriteString("#")
			remain--
		}
		for ; remain > 0; remain-- {
			sb.WriteString(" ")
		}
		sb.WriteString("] ")
		return sb.String()
	}

	return func(inc int) {
		if count == 0 {
			if istty {
				fmt.Printf("%s%s", SAVE_CURSOR, getBarString())
			}
		}
		count += inc
		p := 100 * count / maxCount
		if p <= per {
			return
		}
		per = p
		if istty {
			fmt.Printf("%s%s%s", RESTORE_CURSOR, SAVE_CURSOR, getBarString())
		}

		if per == 100 && !istty {
			fmt.Printf("%s", getBarString())
		}
	}
}
