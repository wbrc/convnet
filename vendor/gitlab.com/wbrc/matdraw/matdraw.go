package matdraw

import (
	"bufio"
	"image"
	"image/color"
	"image/png"
	"math"
	"os"

	"gonum.org/v1/gonum/mat"
)

func DrawRaw(m *mat.Dense, name string) error {
	outputFile, err := os.Create(name)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	buf := bufio.NewWriter(outputFile)
	defer buf.Flush()

	return png.Encode(buf, drawRaw(m))
}

func drawRaw(m *mat.Dense) image.Image {
	rows, cols := m.Dims()

	clone := mat.NewDense(rows, cols, nil)
	clone.CloneFrom(m)
	norm(clone.RawMatrix().Data)

	img := image.NewGray16(image.Rect(0, 0, cols, rows))
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			img.SetGray16(c, r, color.Gray16{uint16(65535 * clone.At(r, c))})
		}
	}
	return img
}

func norm(p []float64) {
	shift(p)
	scale(p)
}

func scale(p []float64) {
	max := getMax(p)
	for i := range p {
		p[i] /= max
	}
}

func shift(p []float64) {
	min := getMin(p)
	for i := range p {
		p[i] -= min
	}
}

func getMin(p []float64) float64 {
	min := p[0]
	for _, e := range p[1:] {
		min = math.Min(min, e)
	}
	return min
}

func getMax(p []float64) float64 {
	max := p[0]
	for _, e := range p[1:] {
		max = math.Max(max, e)
	}
	return max
}
