module gitlab.com/wbrc/convnet

go 1.12

require (
	github.com/petar/GoMNIST v0.0.0-20150320212226-2fbe10d0fa63
	gitlab.com/wbrc/matdraw v0.0.0-20190723101011-3c168901f433
	gitlab.com/wbrc/progress v0.1.0
	gonum.org/v1/gonum v0.0.0-20190723091218-9cfd3e46f14f
	gonum.org/v1/netlib v0.0.0-20190331212654-76723241ea4e
)
